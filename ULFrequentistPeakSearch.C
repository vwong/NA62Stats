//===================================================================================================
// Example macro to prepare RooWorkspace and to set an upper limit on a XS or BR or XS*BR,...
//-----------------------------------------------------------------------
// Model created in wswrite(): 
// - Signal shape is a gaussian 
// - Background shape is a 2nd order polynomial
// - Background yield modeled in the on/off approach
// ----------------------------------------------------------------------
//  Upper limit computation in HypoTestInv, RunInverter, AnalyzeResult
//  adapted from RooStats tutorial StandardHypoTestInvDemo.C 
//===================================================================================================
// Author: Roberta Volpe
//=================================================================================================== 

#include "RooExtendPdf.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooPoisson.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooChebychev.h"
#include "RooPolynomial.h"
#include "RooLandau.h"
#include "RooExponential.h"
#include "RooGamma.h"
#include "RooLognormal.h"
#include "RooAddPdf.h"
#include "RooWorkspace.h"
#include "RooPlot.h"

#include "TObject.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "Riostream.h"
#include "TSystem.h"

#include "RooRandom.h"
#include "RooGlobalFunc.h"
#include "RooProdPdf.h"
#include "RooWorkspace.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "TStopwatch.h"

#include "RooPlot.h"
#include "RooMsgService.h"
#include "RooStats/ModelConfig.h"


#include "RooStats/FrequentistCalculator.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/HypoTestPlot.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/HypoTestInverterPlot.h"

using namespace RooFit;
using namespace RooStats;
using namespace std;

void wswrite(Int_t imass, TString wspath);

HypoTestInverterResult * RunInverter(RooWorkspace * w,
				     const char * modelSBName, const char * modelBName,
				     const char * dataName,
				     int npoints, double poimin, double poimax, int ntoys
				     );
void AnalyzeResult( HypoTestInverterResult * r,
		    int npoints,
		    const char * fileNameBase = 0,
		    //const char * dataName = 0,
		    TString outdir = "");

void HypoTestInv(const char * infile,
		 const char * wsName,
		 const char * modelSBName,
		 const char * modelBName,
		 const char * dataName,
		 int npoints,
		 double poimin,
		 double poimax,
		 int ntoys,
		 TString outdir
		 );

void ULFrequentistPeakSearch(){
  for(Int_t i=0; i<10; i++){
    int imass = 100*i;   
    char wspath[150];
    TString wsoutdir = "ws/";
    gSystem->Exec("mkdir "+wsoutdir);
    sprintf(wspath,wsoutdir+"Fake_m200_m%d",int(imass)); 
     
    wswrite(imass,wspath);
    Double_t mass = imass;
    Int_t ns=-1;
    Double_t smin,smax;
    if(imass==100){
      ns = 20;
      smin = 0.05;
      smax = 1; 
    }else if(imass >= 0 && imass <= 200){
      ns = 20;
      smin = 0.05;
      smax = 1;
    }else if(imass > 200 && imass <= 500){
      ns = 20;
      smin = 0.1;
      smax = 2;    
    }else if(imass>500 && imass<=800){
      ns = 20;
      smin = 0.2;
      smax = 4;  
    }else if(imass>800 && imass<1100){
      ns = 20;
      smin = 0.3;
      smax = 6;  
    }
    /*
    char wsname[150];
    TString wsoutdir = "../ws/";
    gSystem->Exec("mkdir "+wsoutdir);
    sprintf(wsname,wsoutdir+"Fake_m200_m%d.root",int(imass)); 
    */
    TString uloutdir = "UL/";
    gSystem->Exec("mkdir "+uloutdir);
    
    HypoTestInv(TString(wspath),"w",
		"SB_model","B_model",
		"fakeDataSB",
		ns,smin,smax,1000,	
		uloutdir);
  }

  
}


void wswrite(Int_t imass=300,TString wspath="ws0"){
  

  // Define the workspace 
  RooWorkspace* w = new RooWorkspace("w");

  // Define the observable, a discriminant variable
  // In this example it is an invariant mass
  RooRealVar Minv("Minv","Minv",0,1,"GeV^{2}/c^{4}");

  // Parameter of interest:
  RooRealVar XSBRpoi("XSBRpoi","XSBRpoi",1,0,100) ;  // 

  //--------------------------------------------------------------------------------
  //------------------------ Define the background model ---------------------------
  //--------------------------------------------------------------------------------
  //=================================================================
  //a) Define the model for the background yield
  //   tau * b0 = number of expected background events in the "control region/ sidebands/..."
  //   b0 = number of expected background events in the signal region
  //   see  Robert D. Cousins, James T. Linnemann, Jordan Tucker,  arXiv:physics/0702156v4 [physics.data-an], 10.1016/j.nima.2008.07.086
  
  // gb0 = global observable
  // see Kyle Cranmer, arXiv:1503.07622v1 [physics.data-an], CERN-2014-003, pp.267-308
  
  RooRealVar b0("b0","b0",50,0,100) ; // nuisance
  RooRealVar gb0("gb0","gb0",50,0,100) ; // glo
  RooRealVar b0tau("b0tau","b0tau",100,80,120) ; // nuisance

  // Constraint term:  Pois(gb0|b0tau)
  RooPoisson PoissonB0("PoissonB0","PoissonB0",gb0,b0tau);

  // All the objects have to be imported in the workspace
  w->import(PoissonB0);
  w->import(b0); 

  //=================================================================
  //b) Define the model for the background shape
  // example: second order polynomial
  
  Double_t  pa1 = 1;
  Double_t  pa2 = 10;

  // parameters of the polynomial
  // 
  RooRealVar a1("a1","a1",pa1,pa1-0.1*pa1,pa1+0.1*pa1) ;
  RooRealVar a2("a2","a2",pa2,pa2-0.1*pa2,pa2+0.1*pa2) ;

  // each "shape" parameter can be a nuisance parameter, so we need to define the constraint terms:
  // parameters for the constraint terms (Gaussian): mean (expected value for the nuisance) and sigma
  
  // global variables
  RooRealVar a1g("a1g","a1g",pa1) ;
  RooRealVar a2g("a2g","a2g",pa2) ;
  
  // sigma of the gaussian for each nuisance parameter
  RooRealVar sa1("sa1","sa1",0.01*pa1) ; // 1%
  RooRealVar sa2("sa2","sa2",0.01*pa2) ; // 1%
  
  // Constraint terms: Gaus(global obs|mean,sigma)
  RooGaussian gaus_a1("gaus_a1","gaus_a1",a1g,a1,sa1);
  RooGaussian gaus_a2("gaus_a2","gaus_a2",a2g,a2,sa2);

  
  RooPolynomial shapeBkg("shapeBkg","shapeBkg",Minv,RooArgList(a1,a2),1) ;
   
  RooAddPdf bkgPdf("bkgPdf","B-only model",RooArgList(shapeBkg),RooArgList(b0));    // We want an extended likelihood
 
  //-----------------------------------------------------------------------------------------------------
  //---------------------------------------- Define the signal model ------------------------------------
  //-----------------------------------------------------------------------------------------------------
  // It depends on the new particle mass hypothesis, in this example it is taken as input to the macro 

  
  //=======================================================================================
  // a) Define the model for the signal yield
  // nsig = (POI = parameter of interest) * (Norm = normalization factor)
  // - If the analysis searches for a new Kaon decay, the POI is a BR and Norm = 1/SES
  //   here Norm is a single nuisance parameter, but it can be the product of several parameters:
  //   SES = 1/[trigger efficiency * selection efficiency * acceptance * ... * N(K)]  
  
  Double_t normalization = 10;
  RooRealVar Norm("Norm","Norm",normalization,0.01,20) ;
  RooRealVar gNorm("gNorm","gNorm",normalization,0.01,20) ; // global variable for Norm
  
  // The constraint term for the nuisance parameter Norm is a Lognormal distribution Lognorm(gNorm|Norm,k)
  // (because Norm is a non negative multiplicative parameter)
  // The other parameter of the Lognormal is usually defined as k and is related to the
  // uncertainty associated to the parameter
  RooRealVar lnk("lnk","lnk",1.1,1.01,1.2) ; // 1.1 corresponds to a 10% uncertainty
  RooLognormal LogNormNorm("LogNormNorm","LogNormNorm",gNorm,Norm,lnk);

  //=======================================================================================
  // The parameter of interest is XSBRpoi (already defined),
  // but in the likelihood we need the expected number of signal events
  // From the "real" parameter of interest to nsig
  RooFormulaVar nsig("nsig","XSBRpoi*Norm",RooArgList(XSBRpoi,Norm));
  // ------------------------------------------------------------------
  
  //b) Define the shape of the signal in the observable: in this example it is a Gaussian with mean=mass hypothesis and sigma=0.1*mean.
  Double_t Mass = -1;
  Double_t sigmaM = -1;
  Double_t MassPoint = -1;
  Double_t MassHyp = imass;  // input
  MassPoint = imass*0.001 ;  //from MeV/c^2 to GeV/c^2
  if(imass==0)MassPoint = imass+0.01; // temporary fix to have the pdfs always non-negative
  Mass = MassPoint;
  sigmaM =  0.1*MassPoint ;
 
  RooRealVar mean("mean","mean of gaussian",MassPoint,max(0.001,MassPoint-0.2*MassPoint),MassPoint+0.2*MassPoint) ; 
  RooRealVar sigma("sigma","width of gaussian",sigmaM,max(0.001,sigmaM-0.1*sigmaM),0.1*sigmaM+sigmaM) ;
  RooGaussian shapeSig("shapeSig","shapeSig",Minv,mean,sigma);

  RooRealVar gmean("gmean","gmean",MassPoint,max(0.001,MassPoint-0.1*MassPoint),MassPoint+0.1*MassPoint) ;  // Global for mean
  RooRealVar gsigma("gsigma","width of gaussian",sigmaM,max(0.001,sigmaM-0.1*sigmaM),sigmaM+sigmaM) ;  // Global for sigma
  RooRealVar lnksig("lnksig","lnksig",1.1,1.01,1.5) ;  //  k=1.1  ==> 10% of uncertainty for sigma

  // Sigma is a non-negative variable, Lognormal distribution is an appropriate constraint
  RooLognormal LogNormSigma("LogNormSigma","LogNormSigma",gsigma,sigma,lnksig);
  
  RooRealVar sigmamean("sigmamean","sigmamean",MassPoint*0.02,0.001,MassPoint*0.1) ;  // sigma of the Gaussian constraint for the nuisance "mean"
  RooLognormal GausMean("GausMean","GausMean",gmean,mean,sigmamean); // Gaussian constraint for mean 
  
  //----------------
  
  RooAddPdf modelMean("modelMean","modelMean",RooArgList(bkgPdf,shapeSig),RooArgList(b0,nsig)) ;  
  
  RooProdPdf model("model","model",RooArgSet(modelMean				 
					     ,PoissonB0 // Constraint for the nuisance b0
					     ,LogNormNorm // Constraint for the nuisance Norm
					     ,LogNormSigma //
					     ,GausMean //
					     ,gaus_a1
					     ,gaus_a2
					     )
		   );
					     
  char massname[150] = "";
  sprintf(massname,"m_{hyp}= %d MeV",int(MassHyp));


  // Create the RooPlot 
  RooPlot* frame = Minv.frame(Name("frame"),Title(""),Bins(20)) ;
  TCanvas* cb = new TCanvas("cb","cb",1000,700) ;
  cb->cd();
  cb->SetMargin(0.2,0.08,0.2,0.08);
 
  frame->SetTitleSize(0.07,"xy");
  frame->SetYTitle("a.u.");
  frame->SetXTitle("m_{inv} [GeV/c^{2}]");
  frame->SetTitle(massname);
  //  bkgPdf.plotOn(frame,LineColor(kBlue)) ;
  //  model.plotOn(frame,LineColor(kViolet)) ;
  // shapeSig.plotOn(frame,LineColor(kRed)) ;

  w->import(Minv);
  w->import(nsig);
  w->import(shapeSig);
  w->import(bkgPdf);

  w->import(LogNormNorm);
  w->import(LogNormSigma);
  w->import(GausMean);
  w->import(gaus_a1);
  w->import(gaus_a2);
  
  w->defineSet("obs","Minv");
  w->defineSet("poi","XSBRpoi");

  w->defineSet("nuisParams","b0,sigma,mean,Norm,a1,a2");
  w->defineSet("global", "gb0,gmean,gsigma,gNorm,lnk,lnksig,a1g,a2g");
  //=====================================================================
  // All the global observables must be set constant !!!!
  //=====================================================================
  // They are constant in the fit for the PLL but they vary in the toys
  
   
  w->var("gb0")->setConstant(true);  //
  w->var("gNorm")->setConstant(true);
  w->var("a1g")->setConstant(true);
  w->var("a2g")->setConstant(true);
  w->var("gmean")->setConstant(true);
  w->var("gsigma")->setConstant(true);
  
  w->var("lnk")->setConstant(true);
  w->var("lnksig")->setConstant(true);

  //=============  FAKE DATA!!!! =====================
  // they are generated with the bkg distribution
  //==================================================
 
  RooRandom::randomGenerator()->SetSeed(1234); // I want that the data are the same for each mass hypothesis
  RooDataSet * fakeDataB = bkgPdf.generate(Minv,50);  // only-bkg hypothesys
  fakeDataB->SetName("fakeDataB");
  //==================================================

  
  RooRealVar FakeMean("FakeMean","mean of gaussian",0.2) ; // MX=200 MeV 
  RooRealVar FakeSigma("FakeSigma","width of gaussian",0.02) ;  //
  RooGaussian shapeFakeSig("shapeFakeSig","shapeFakeSig",Minv,FakeMean,FakeSigma);
  RooRealVar nFakeSig("nFakeSig","n of sig",3,0,10) ;
  
  RooAddPdf FakeModel("FakeSigPdf","Fake SIG model",RooArgList(bkgPdf,shapeFakeSig),RooArgList(b0,nFakeSig));
  RooDataSet *fakeDataSB = FakeModel.generate(Minv,50);  
  fakeDataSB->SetName("fakeDataSB");
  w->import(*fakeDataSB);
  w->import(*fakeDataB);
  //  fakeDataSB->plotOn(frame);
  bkgPdf.plotOn(frame,LineColor(kBlue)) ;
  model.plotOn(frame,LineColor(kViolet)) ;
  // fakeDataSB->plotOn(frame) ;

  
  frame->Draw();
  
  // TString outdir = "../WS_SmallStats/";
  // gSystem->Exec("mkdir "+outdir);
  // char plotname[150] = "";
  TString plotfilename(TString(wspath)+".pdf");
  //  sprintf(plotname,"Fake_m200_m%d.pdf",int(MassHyp));
  //  cb->SaveAs(outdir+TString(plotname));
  cb->SaveAs(plotfilename);
  
  ModelConfig b_model("B_model", w);
  b_model.SetPdf(model);
  b_model.SetObservables(*w->set("obs"));
  b_model.SetParametersOfInterest(*w->set("poi"));
  b_model.SetNuisanceParameters(*w->set("nuisParams"));
  b_model.SetGlobalObservables(*w->set("global"));
  w->var("XSBRpoi")->setVal(0.0); 
  b_model.SetSnapshot(*w->set("poi"));

  ModelConfig sb_model("SB_model", w);
  sb_model.SetPdf(model);
  sb_model.SetObservables(*w->set("obs"));
  sb_model.SetParametersOfInterest(*w->set("poi"));
  w->var("XSBRpoi")->setVal(1);
  sb_model.SetSnapshot(*w->set("poi"));
  sb_model.SetNuisanceParameters(*w->set("nuisParams"));
  sb_model.SetConstraintParameters(*w->set("nuisParams"));
  sb_model.SetGlobalObservables(*w->set("global"));
  w->import(sb_model) ;
  w->import(b_model) ;
  w->Print() ;
  
  // char wsname[150] = "";
  //sprintf(wsname,"Fake_m200_ws_m%d.root",int(MassHyp));
  TString wsfilename(TString(wspath)+".root");
  w->writeToFile(wsfilename) ;
  //  w->writeToFile(wsname) ;  
  gDirectory->Add(w) ;
  return;
}

/*
HypoTestInverterResult * RunInverter(RooWorkspace * w,
				     const char * modelSBName, const char * modelBName,
				     const char * dataName,
				     int npoints, double poimin, double poimax, int ntoys
				     );
void AnalyzeResult( HypoTestInverterResult * r,
		    int npoints,
		    const char * fileNameBase = 0,
		    //const char * dataName = 0,
		    TString outdir = "");

*/
void HypoTestInv(const char * infile = 0,
		 const char * wsName = "combined",
		 const char * modelSBName = "ModelConfig",
		 const char * modelBName = "",
		 const char * dataName = "obsData",
		 int npoints = 6,
		 double poimin = 0,
		 double poimax = 5,
		 int ntoys=1000,
		 TString outdir= "test"
		 ){
  

  TString filename(infile);
  //TString filename;
  filename = infile+TString(".root");
  // sprintf(wsname);
  
  // Try to open the file
  TFile *file = TFile::Open(filename);
  // if input file was specified byt not found, quit
  if(!file ){
    cout <<" Input file " << filename << " is not found" << endl;
    return;
  }
  
  RooStats::UseNLLOffset(true);
  RooWorkspace * w = dynamic_cast<RooWorkspace*>( file->Get(wsName) );
  HypoTestInverterResult * r = 0;
  std::cout << w << "\t" << filename << std::endl;
  if (w != NULL) {
    r = RunInverter(w, modelSBName, modelBName,
		    dataName,
		    npoints, poimin, poimax,
		    ntoys);
    
    if (!r) {
      std::cerr << "Error running the HypoTestInverter - Exit " << std::endl;
      return;
    }
  }
  AnalyzeResult( r,npoints, infile, outdir );
  return;
}

void AnalyzeResult( HypoTestInverterResult * r,
		    int npoints,
		    const char * fileNameBase,
		    TString outdir){
  
  // analyze result produced by the inverter
   double lowerLimit = 0;
   double llError = 0;
   if (r->IsTwoSided()) {
     lowerLimit = r->LowerLimit();
     llError = r->LowerLimitEstimatedError();
   }
   double upperLimit = r->UpperLimit();
   double ulError = r->UpperLimitEstimatedError();
   std::cout <<  "ws filename: "<< fileNameBase   <<"  obs. upper limit: " << upperLimit << " \t" << ulError << std::endl;
  
   // compute expected limit
   std::cout << "Expected upper limits, using the B (alternate) model : " << std::endl;
   std::cout << " expected limit (median) " << r->GetExpectedUpperLimit(0) << std::endl;
   std::cout << " expected limit (-1 sig) " << r->GetExpectedUpperLimit(-1) << std::endl;
   std::cout << " expected limit (+1 sig) " << r->GetExpectedUpperLimit(1) << std::endl;
   std::cout << " expected limit (-2 sig) " << r->GetExpectedUpperLimit(-2) << std::endl;
   std::cout << " expected limit (+2 sig) " << r->GetExpectedUpperLimit(2) << std::endl;
   
   std::cout << "ws filename: "<< fileNameBase   <<"   obs-expected-m1s-p1s-m2s-p2s: " <<
     upperLimit <<"\t"<<
     r->GetExpectedUpperLimit(0) <<"\t"<<
     r->GetExpectedUpperLimit(-1) <<"\t"<<
     r->GetExpectedUpperLimit(1) <<"\t"<< 
     r->GetExpectedUpperLimit(-2) <<"\t"<<
     r->GetExpectedUpperLimit(2) <<"\t"<<  std::endl;
   Info("SetUL","detailed output will be written in output result file");
      // }
   TString mResultFileName;
   mResultFileName = outdir+"freqPLL_";
   TString name = fileNameBase;
   name.Replace(0, name.Last('/')+1, "");
   mResultFileName += name;

 
   const char * resultName = r->GetName();
   TString plotTitle = TString::Format("Freq CL Scan for workspace %s",resultName);
   HypoTestInverterPlot *plot = new HypoTestInverterPlot("HTI_Result_Plot",plotTitle,r);
   // plot in a new canvas with style
   TString c1Name = outdir+"Freq_Scan";
   TCanvas * c1 = new TCanvas(c1Name);
   c1->SetLogy(false);
   plot->Draw("CLb 2CL");  // plot all and Clb
   c1->SaveAs(TString::Format("%s.pdf",mResultFileName.Data()));
   c1->SaveAs(TString::Format("%s.C",mResultFileName.Data()));
   c1->SetLogy(true);
   c1->SaveAs(TString::Format("%s_log.pdf",mResultFileName.Data()));
   c1->SaveAs(TString::Format("%s_log.C",mResultFileName.Data()));
   const int nEntries = r->ArraySize();
   gPad = c1;

   
   TString uldistFile = "RULDist.root";
   TObject * ulDist = 0;
   bool existULDist = !gSystem->AccessPathName(uldistFile);
   if (existULDist) {
     TFile * fileULDist = TFile::Open(uldistFile);
     if (fileULDist) ulDist= fileULDist->Get("RULDist");
   }
   TString ResultFile = TString::Format("%s.root",mResultFileName.Data());
   TFile * fileOut = new TFile(ResultFile,"RECREATE");
   r->Write();
   if (ulDist) ulDist->Write();
   Info("StandardHypoTestInvDemo","HypoTestInverterResult has been written in the file %s",mResultFileName.Data());   
   fileOut->Close();

   
}


// internal routine to run the inverter
HypoTestInverterResult * RunInverter(RooWorkspace * w,
                                       const char * modelSBName, const char * modelBName,
				       const char * dataName,  // now it is the pdf of S+B
				       int npoints, double poimin, double poimax,
                                       int ntoys
				       ){

  std::cout << "Running HypoTestInverter on the workspace " << w->GetName() << std::endl;
  
  w->Print();
  

  cout <<"*************** -------  dataName "<<  dataName <<endl;
  RooAbsData * data = w->data(dataName);
  data->Print("v");
 
  if (!data) {
    Error("HypoTest","Not existing data %s",dataName);
    return 0;
  }
   else
     std::cout << "Using data set " << dataName << std::endl;
 
  RooAbsData::setDefaultStorageType(RooAbsData::Vector);
  data->convertToVectorStore() ;

  

  // get models from WS
  // get the modelConfig out of the file
  ModelConfig* bModel = (ModelConfig*) w->obj(modelBName);
  ModelConfig* sbModel = (ModelConfig*) w->obj(modelSBName);
  
  
  cout <<" models taken "<<  endl;
  if (!sbModel) {
    Error("HypoTest","Not existing ModelConfig %s",modelSBName);
    return 0;
  }
  // check the model
  if (!sbModel->GetPdf()) {
    Error("HypoTest","Model %s has no pdf ",modelSBName);
    return 0;
  }
  if (!sbModel->GetParametersOfInterest()) {
    Error("HypoTest","Model %s has no poi ",modelSBName);
    return 0;
  }
  if (!sbModel->GetObservables()) {
    Error("HypoTestInv","Model %s has no observables ",modelSBName);
    return 0;
  }
  if (!sbModel->GetSnapshot() ) {
    Info("HypoTestInv","Model %s has no snapshot  - make one using model poi",modelSBName);
    sbModel->SetSnapshot( *sbModel->GetParametersOfInterest() );
  }
  
  // We can define only the S+B model, the B-only is created here 
  if (!bModel || bModel == sbModel) {
    Info("HypoTestInv","The background model %s does not exist",modelBName);
    Info("HypoTestInv","Copy it from ModelConfig %s and set POI to zero",modelSBName);
    bModel = (ModelConfig*) sbModel->Clone();
    bModel->SetName(TString(modelSBName)+TString("_with_poi_0"));
    RooRealVar * var = dynamic_cast<RooRealVar*>(bModel->GetParametersOfInterest()->first());
    if (!var) return 0;
    double oldval = var->getVal();
    var->setVal(0);
    bModel->SetSnapshot( RooArgSet(*var)  );
    var->setVal(oldval);
  }

  else {
    if (!bModel->GetSnapshot() ) {
      Info("HypoTestInv","Model %s has no snapshot  - make one using model poi and 0 values ",modelBName);
      RooRealVar * var = dynamic_cast<RooRealVar*>(bModel->GetParametersOfInterest()->first());
      if (var) {
	double oldval = var->getVal();
	var->setVal(0);
	bModel->SetSnapshot( RooArgSet(*var)  );
	var->setVal(oldval);
      }
      else {
	Error("HypoTestInv","Model %s has no valid poi",modelBName);
	return 0;
      }
    }
  }
  // check model  has global observables when there are nuisance pdf
  // for the hybrid case the globals are not needed
 
  bool hasNuisParam = (sbModel->GetNuisanceParameters() && sbModel->GetNuisanceParameters()->getSize() > 0);
  bool hasGlobalObs = (sbModel->GetGlobalObservables() && sbModel->GetGlobalObservables()->getSize() > 0);
  if (hasNuisParam && !hasGlobalObs ) {
    // try to see if model has nuisance parameters first
    RooAbsPdf * constrPdf = RooStats::MakeNuisancePdf(*sbModel,"nuisanceConstraintPdf_sbmodel");
    if (constrPdf) {
      Warning("HypoTestInv","Model %s has nuisance parameters but no global observables associated",sbModel->GetName());
      Warning("HypoTestInv","\tThe effect of the nuisance parameters will not be treated correctly ");
    }
  }
  // save all initial parameters of the model including the global observables
  RooArgSet initialParameters;
  RooArgSet * allParams = sbModel->GetPdf()->getParameters(*data);
  allParams->snapshot(initialParameters);
  delete allParams;
  
  // run first a data fit
  
  const RooArgSet * poiSet = sbModel->GetParametersOfInterest();
  RooRealVar *poi = (RooRealVar*)poiSet->first();
  
  std::cout << "HypoTestInv : POI initial value:   " << poi->GetName() << " = " << poi->getVal()   << std::endl;
  
  // fit the data first (need to use constraint )
  TStopwatch tw;
  
  bool doFit = true;

  //----------------------------------------
  // Take the nominal values of the parameters as starting point, do not fit using data !
  // This can give strange results with small samples
  doFit = false;
  //----------------------------------------
  
  double poihat = 0;
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit");
 
  if (doFit)  {
    Info( "HypoTestInv"," Doing a first fit to the observed data ");
    RooArgSet constrainParams;
    if (sbModel->GetNuisanceParameters() ) constrainParams.add(*sbModel->GetNuisanceParameters());
    RooStats::RemoveConstantParameters(&constrainParams);
      tw.Start();
      RooFitResult * fitres = sbModel->GetPdf()->fitTo(*data,InitialHesse(false), Hesse(false),
                                                       Minimizer("Minuit","Migrad"), Strategy(0), PrintLevel(0),
						       Constrain(constrainParams), Save(true), Offset(RooStats::IsNLLOffset()) );
      if (fitres->status() != 0) {
	Warning("HypoTestInv","Fit to the model failed - try with strategy 1 and perform first an Hesse computation");
	fitres = sbModel->GetPdf()->fitTo(*data,InitialHesse(true), Hesse(false),Minimizer("Minuit","Migrad"), Strategy(1),
					   PrintLevel(1), Constrain(constrainParams),
					  Save(true), Offset(RooStats::IsNLLOffset()) );
      }
      if (fitres->status() != 0)
	Warning("HypoTestInv"," Fit still failed - continue anyway.....");
     
      poihat  = poi->getVal();
      std::cout << "HypoTestInv - Best Fit value : " << poi->GetName() << " = "
                << poihat << " +/- " << poi->getError() << std::endl;
      std::cout << "Time for fitting : "; tw.Print();
      
      //save best fit value in the poi snapshot
      sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
      std::cout << "HypoTestInv: snapshot of S+B Model " << sbModel->GetName()
                << " is set to the best fit value" << std::endl;
      
  }

  
  ProfileLikelihoodTestStat profll(*sbModel->GetPdf());
  profll.SetOneSided(true); // 1-sided
  profll.SetMinimizer("Minuit");  
  profll.EnableDetailedOutput();  
  profll.SetReuseNLL(true);  // Negative Log Likelihood
  
  if (true) {
    profll.SetStrategy(0);
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
  }
 
  HypoTestCalculatorGeneric *  hc = 0;
  
  hc = new FrequentistCalculator(*data, *bModel, *sbModel);
  
  TestStatistic * testStat = 0;
  testStat = &profll;
  hc->UseSameAltToys();
  ToyMCSampler *toymcs = (ToyMCSampler*)hc->GetTestStatSampler();
  
  toymcs->SetTestStatistic(testStat);
  
  ((FrequentistCalculator*) hc)->SetToys(ntoys,ntoys/2);
  // store also the fit information for each poi point used by calculator based on toys
  ((FrequentistCalculator*) hc)->StoreFitInfo(true);
  
  // Get the result
  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration);
  HypoTestInverter calc(*hc);
  calc.SetConfidenceLevel(0.95);   
  calc.UseCLs(true);
  calc.SetVerbose(true);
  
  ProofConfig pc(*w, 0, "", kFALSE);
  toymcs->SetProofConfig(&pc);    // enable proof


 
  
  if (npoints > 0) {
      if (poimin > poimax) {
         // if no min/max given scan between MLE and +4 sigma
         poimin = int(poihat);
         poimax = int(poihat +  4 * poi->getError());
      }
      std::cout << "Doing a fixed scan  in interval : " << poimin << " , " << poimax << std::endl;
      calc.SetFixedScan(npoints,poimin,poimax);
   }
   else {
      //poi->setMax(10*int( (poihat+ 10 *poi->getError() )/10 ) );
      std::cout << "Doing an  automatic scan  in interval : " << poi->getMin() << " , " << poi->getMax() << std::endl;
   }

   tw.Start();
   HypoTestInverterResult * r = calc.GetInterval();
   std::cout << "Time to perform limit scan \n";
   tw.Print();


   // Save the results in a root file:
   
  
   return r;
}




void ReadResult(const char * fileName, const char * resultName="", bool useCLs=true) {
   // read a previous stored result from a file given the result name
   HypoTestInv(fileName, resultName,"","","",0,0,useCLs);
}


#ifdef USE_AS_MAIN
int main() {
    HypoTestInv();
}
#endif

