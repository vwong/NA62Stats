///==========================================================================
// set an upper limit using the FrequentistCalculator and
// one-sided profile likelihood as a test statistic 
// Heavily based on the RooStats tutorial StandardHypoTestInvDemo.C 
// (selection of code related to frequentist calculator and profile likelihood as test statistic)
///==========================================================================

#include "TFile.h"
#include "RooWorkspace.h"
#include "RooAbsPdf.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooStats/ModelConfig.h"
#include "RooRandom.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TROOT.h"
#include "TSystem.h"

#include "RooStats/FrequentistCalculator.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/HypoTestPlot.h"
#include "RooStats/ProfileLikelihoodTestStat.h"
#include "RooStats/HypoTestInverter.h"
#include "RooStats/HypoTestInverterResult.h"
#include "RooStats/HypoTestInverterPlot.h"

using namespace RooFit;
using namespace RooStats;
using namespace std;

HypoTestInverterResult * RunInverter(RooWorkspace * w,
				     const char * modelSBName, const char * modelBName,
				     const char * dataName,
				     int npoints, double poimin, double poimax, int ntoys
				     );
void AnalyzeResult( HypoTestInverterResult * r,
		    int npoints,
		    const char * fileNameBase = 0,
		    //const char * dataName = 0,
		    TString outdir = "");


void HypoTestInv(const char * infile = 0,
		 const char * wsName = "w",
		 const char * modelSBName = "ModelConfig",
		 const char * modelBName = "",
		 const char * dataName = "obsData",
		 int npoints = 6,
		 double poimin = 0,
		 double poimax = 5,
		 int ntoys=1000,
		 TString outdir= "test"
		 ){
  

  TString filename(infile);
  filename = infile;
   
  // Try to open the file
  TFile *file = TFile::Open(filename);
  // if input file was specified byt not found, quit
  if(!file ){
    cout <<" Input file " << filename << " is not found" << endl;
    return;
  }
  
  RooStats::UseNLLOffset(true);
  RooWorkspace * w = dynamic_cast<RooWorkspace*>( file->Get(wsName) );
  HypoTestInverterResult * r = 0;
  std::cout << w << "\t" << filename << std::endl;
  if (w != NULL) {
    r = RunInverter(w, modelSBName, modelBName,
		    dataName,
		    npoints, poimin, poimax,
		    ntoys);
    
    if (!r) {
      std::cerr << "Error running the HypoTestInverter - Exit " << std::endl;
      return;
    }
  }
  AnalyzeResult( r,npoints, infile, outdir );
  return;
}

void AnalyzeResult( HypoTestInverterResult * r,
		    int npoints,
		    const char * fileNameBase,
		    TString outdir){
  
  // analyze result produced by the inverter
   double lowerLimit = 0;
   double llError = 0;
   if (r->IsTwoSided()) {
     lowerLimit = r->LowerLimit();
     llError = r->LowerLimitEstimatedError();
   }
   double upperLimit = r->UpperLimit();
   double ulError = r->UpperLimitEstimatedError();
   std::cout <<  "ws filename: "<< fileNameBase   <<"  obs. upper limit: " << upperLimit << " \t" << ulError << std::endl;
  
   // compute expected limit
   std::cout << "Expected upper limits, using the B (alternate) model : " << std::endl;
   std::cout << " expected limit (median) " << r->GetExpectedUpperLimit(0) << std::endl;
   std::cout << " expected limit (-1 sig) " << r->GetExpectedUpperLimit(-1) << std::endl;
   std::cout << " expected limit (+1 sig) " << r->GetExpectedUpperLimit(1) << std::endl;
   std::cout << " expected limit (-2 sig) " << r->GetExpectedUpperLimit(-2) << std::endl;
   std::cout << " expected limit (+2 sig) " << r->GetExpectedUpperLimit(2) << std::endl;
   
   std::cout << "ws filename: "<< fileNameBase   <<"   obs-expected-m1s-p1s-m2s-p2s: " <<
     upperLimit <<"\t"<<
     r->GetExpectedUpperLimit(0) <<"\t"<<
     r->GetExpectedUpperLimit(-1) <<"\t"<<
     r->GetExpectedUpperLimit(1) <<"\t"<< 
     r->GetExpectedUpperLimit(-2) <<"\t"<<
     r->GetExpectedUpperLimit(2) <<"\t"<<  std::endl;
   Info("SetUL","detailed output will be written in output result file");
      // }
   TString mResultFileName;
   mResultFileName = outdir+"freqPLL_";
   TString name = fileNameBase;
   name.Replace(0, name.Last('/')+1, "");
   mResultFileName += name;
  
   const char * resultName = r->GetName();
   TString plotTitle = TString::Format("Freq CL Scan for workspace %s",resultName);
   HypoTestInverterPlot *plot = new HypoTestInverterPlot("HTI_Result_Plot",plotTitle,r);
   // plot in a new canvas with style
   TString c1Name = outdir+"Freq_Scan";
   TCanvas * c1 = new TCanvas(c1Name);
   c1->SetLogy(false);
   plot->Draw("CLb 2CL");  // plot all and Clb
   c1->SaveAs(TString::Format("%s.pdf",mResultFileName.Data()));
   c1->SaveAs(TString::Format("%s.C",mResultFileName.Data()));
   c1->SetLogy(true);
   c1->SaveAs(TString::Format("%s_log.pdf",mResultFileName.Data()));
   c1->SaveAs(TString::Format("%s_log.C",mResultFileName.Data()));
   const int nEntries = r->ArraySize();
   gPad = c1;

   //------------------------------------------------
   // Save the results also in a root file

   TString uldistFile = "RULDist.root";
   TObject * ulDist = 0;
   bool existULDist = !gSystem->AccessPathName(uldistFile);
   if (existULDist) {
     TFile * fileULDist = TFile::Open(uldistFile);
     if (fileULDist) ulDist= fileULDist->Get("RULDist");
   }
   TString ResultFile = TString::Format("%s.root",mResultFileName.Data());
   TFile * fileOut = new TFile(ResultFile,"RECREATE");
   r->Write();
   if (ulDist) ulDist->Write();
   Info("StandardHypoTestInvDemo","HypoTestInverterResult has been written in the file %s",mResultFileName.Data());   
   fileOut->Close();
   
   
}


// internal routine to run the inverter
HypoTestInverterResult * RunInverter(RooWorkspace * w,
                                       const char * modelSBName, const char * modelBName,
				       const char * dataName,  // now it is the pdf of S+B
				       int npoints, double poimin, double poimax,
                                       int ntoys
				       ){

  std::cout << "Running HypoTestInverter on the workspace " << w->GetName() << std::endl;
  
  w->Print();
  

  cout <<"*************** -------  dataName "<<  dataName <<endl;
  RooAbsData * data = w->data(dataName);
  data->Print("v");
 
  if (!data) {
    Error("HypoTest","Not existing data %s",dataName);
    return 0;
  }
   else
     std::cout << "Using data set " << dataName << std::endl;
 
  RooAbsData::setDefaultStorageType(RooAbsData::Vector);
  data->convertToVectorStore() ;

  

  // get models from WS
  // get the modelConfig out of the file
  ModelConfig* bModel = (ModelConfig*) w->obj(modelBName);
  ModelConfig* sbModel = (ModelConfig*) w->obj(modelSBName);
  
  
  cout <<" models taken "<<  endl;
  if (!sbModel) {
    Error("HypoTest","Not existing ModelConfig %s",modelSBName);
    return 0;
  }
  // check the model
  if (!sbModel->GetPdf()) {
    Error("HypoTest","Model %s has no pdf ",modelSBName);
    return 0;
  }
  if (!sbModel->GetParametersOfInterest()) {
    Error("HypoTest","Model %s has no poi ",modelSBName);
    return 0;
  }
  if (!sbModel->GetObservables()) {
    Error("HypoTestInv","Model %s has no observables ",modelSBName);
    return 0;
  }
  if (!sbModel->GetSnapshot() ) {
    Info("HypoTestInv","Model %s has no snapshot  - make one using model poi",modelSBName);
    sbModel->SetSnapshot( *sbModel->GetParametersOfInterest() );
  }
  

  if (!bModel || bModel == sbModel) {
    Info("HypoTestInv","The background model %s does not exist",modelBName);
    Info("HypoTestInv","Copy it from ModelConfig %s and set POI to zero",modelSBName);
    bModel = (ModelConfig*) sbModel->Clone();
    bModel->SetName(TString(modelSBName)+TString("_with_poi_0"));
    RooRealVar * var = dynamic_cast<RooRealVar*>(bModel->GetParametersOfInterest()->first());
    if (!var) return 0;
    double oldval = var->getVal();
    var->setVal(0);
    bModel->SetSnapshot( RooArgSet(*var)  );
    var->setVal(oldval);
  }
  else {
    if (!bModel->GetSnapshot() ) {
      Info("HypoTestInv","Model %s has no snapshot  - make one using model poi and 0 values ",modelBName);
      RooRealVar * var = dynamic_cast<RooRealVar*>(bModel->GetParametersOfInterest()->first());
      if (var) {
	double oldval = var->getVal();
	var->setVal(0);
	bModel->SetSnapshot( RooArgSet(*var)  );
	var->setVal(oldval);
      }
      else {
	Error("HypoTestInv","Model %s has no valid poi",modelBName);
	return 0;
      }
    }
  }
  // check model  has global observables when there are nuisance pdf
  // for the hybrid case the globals are not needed
 
  bool hasNuisParam = (sbModel->GetNuisanceParameters() && sbModel->GetNuisanceParameters()->getSize() > 0);
  bool hasGlobalObs = (sbModel->GetGlobalObservables() && sbModel->GetGlobalObservables()->getSize() > 0);
  if (hasNuisParam && !hasGlobalObs ) {
    // try to see if model has nuisance parameters first
    RooAbsPdf * constrPdf = RooStats::MakeNuisancePdf(*sbModel,"nuisanceConstraintPdf_sbmodel");
    if (constrPdf) {
      Warning("HypoTestInv","Model %s has nuisance parameters but no global observables associated",sbModel->GetName());
      Warning("HypoTestInv","\tThe effect of the nuisance parameters will not be treated correctly ");
    }
  }
  // save all initial parameters of the model including the global observables
  RooArgSet initialParameters;
  RooArgSet * allParams = sbModel->GetPdf()->getParameters(*data);
  allParams->snapshot(initialParameters);
  delete allParams;
  
  // run first a data fit
  
  const RooArgSet * poiSet = sbModel->GetParametersOfInterest();
  RooRealVar *poi = (RooRealVar*)poiSet->first();
  
  std::cout << "HypoTestInv : POI initial value:   " << poi->GetName() << " = " << poi->getVal()   << std::endl;
  
  // fit the data first (need to use constraint )
  TStopwatch tw;
  
  // Sometimes, with very low statistics, with the initial fit there is 
  // a dependence of the expected upper limit from the observed dataset.
  // No fit in this example:
  bool doFit = false;
  double poihat = 0;
  ROOT::Math::MinimizerOptions::SetDefaultMinimizer("Minuit");
 
  if (doFit)  {
    Info( "HypoTestInv"," Doing a first fit to the observed data ");
    RooArgSet constrainParams;
    if (sbModel->GetNuisanceParameters() ) constrainParams.add(*sbModel->GetNuisanceParameters());
    RooStats::RemoveConstantParameters(&constrainParams);
      tw.Start();
      RooFitResult * fitres = sbModel->GetPdf()->fitTo(*data,InitialHesse(false), Hesse(false),
                                                       Minimizer("Minuit","Migrad"), Strategy(0), PrintLevel(0),
						       Constrain(constrainParams), Save(true), Offset(RooStats::IsNLLOffset()) );
      if (fitres->status() != 0) {
	Warning("HypoTestInv","Fit to the model failed - try with strategy 1 and perform first an Hesse computation");
	fitres = sbModel->GetPdf()->fitTo(*data,InitialHesse(true), Hesse(false),Minimizer("Minuit","Migrad"), Strategy(1),
					   PrintLevel(1), Constrain(constrainParams),
					  Save(true), Offset(RooStats::IsNLLOffset()) );
      }
      if (fitres->status() != 0)
	Warning("HypoTestInv"," Fit still failed - continue anyway.....");
     
      poihat  = poi->getVal();
      std::cout << "HypoTestInv - Best Fit value : " << poi->GetName() << " = "
                << poihat << " +/- " << poi->getError() << std::endl;
      std::cout << "Time for fitting : "; tw.Print();
      
      //save best fit value in the poi snapshot
      sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
      std::cout << "HypoTestInv: snapshot of S+B Model " << sbModel->GetName()
                << " is set to the best fit value" << std::endl;
      
  }

 
  ProfileLikelihoodTestStat profll(*sbModel->GetPdf());
  profll.SetOneSided(true);
  profll.SetMinimizer("Minuit");
  profll.EnableDetailedOutput();  
  profll.SetReuseNLL(true);
  
  if (true) {
    profll.SetStrategy(0);
    ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
  }
 
  HypoTestCalculatorGeneric *  hc = 0;
  
  hc = new FrequentistCalculator(*data, *bModel, *sbModel);
  
  TestStatistic * testStat = 0;
  testStat = &profll;
  
  ToyMCSampler *toymcs = (ToyMCSampler*)hc->GetTestStatSampler();
  
  toymcs->SetTestStatistic(testStat);
  
  ((FrequentistCalculator*) hc)->SetToys(ntoys,ntoys/2);
  // store also the fit information for each poi point used by calculator based on toys
  ((FrequentistCalculator*) hc)->StoreFitInfo(true);
  
  // Get the result
  RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration);
  HypoTestInverter calc(*hc);
  calc.SetConfidenceLevel(0.95);   
  calc.UseCLs(true);
  calc.SetVerbose(true);
  
  ProofConfig pc(*w, 0, "", kFALSE);
  toymcs->SetProofConfig(&pc);    // enable proof
 
  if (npoints > 0) {
      if (poimin > poimax) {
         // if no min/max given scan between MLE and +4 sigma
         poimin = int(poihat);
         poimax = int(poihat +  4 * poi->getError());
      }
      std::cout << "Doing a fixed scan  in interval : " << poimin << " , " << poimax << std::endl;
      calc.SetFixedScan(npoints,poimin,poimax);
   }
   else {
      //poi->setMax(10*int( (poihat+ 10 *poi->getError() )/10 ) );
      std::cout << "Doing an  automatic scan  in interval : " << poi->getMin() << " , " << poi->getMax() << std::endl;
   }

   tw.Start();
   HypoTestInverterResult * r = calc.GetInterval();
   std::cout << "Time to perform limit scan \n";
   tw.Print();

   return r;
}



void ReadResult(const char * fileName, const char * resultName="", bool useCLs=true) {
   // read a previous stored result from a file given the result name

   HypoTestInv(fileName, resultName,"","","",0,0,useCLs);
}


#ifdef USE_AS_MAIN
int main() {
    HypoTestInv();
}
#endif




